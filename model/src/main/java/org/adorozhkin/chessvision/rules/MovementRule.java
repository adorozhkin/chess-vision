package org.adorozhkin.chessvision.rules;

import org.adorozhkin.chessvision.model.Board;
import org.adorozhkin.chessvision.model.Color;
import org.adorozhkin.chessvision.model.Coordinate;
import org.adorozhkin.chessvision.model.Figure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class MovementRule {
    private final Board board;

    public MovementRule(Board board) {
        this.board = board;
    }

    public List<Coordinate> getPossibleMoves(Coordinate cord) {
        Optional<List<Coordinate>> result = board.getCell(cord).getFigure().map(f -> getPossibleMoves(f, cord));
        return result.orElse(Collections.emptyList());
    }

    private List<Coordinate> getPossibleMoves(Figure f, Coordinate cord) {
        switch (f.getType()) {
            case BISHOP:
                return getBishopMoves(f.getColor(), cord);
            case KING:
                return getKingMoves(f.getColor(), cord);
            case KNIGHT:
                return getKnightMoves(f.getColor(), cord);
            case PAWN:
                return getPawnMoves(f.getColor(), cord);
            case QUEEN:
                return getQueenMoves(f.getColor(), cord);
            case ROOK:
                return getRookMoves(f.getColor(), cord);
        }
        return null;
    }

    private List<Coordinate> getBishopMoves(Color color, Coordinate cord) {
        List<Coordinate> initialMoves = new ArrayList<>();
        IntStream.range(0, board.getSize()).forEach(i -> {
            initialMoves.add(new Coordinate(i, i));
            initialMoves.add(new Coordinate(-i, -i));
        });
        return filterOverjump(transformMoves(initialMoves, color, cord), cord);
    }

    private List<Coordinate> getKingMoves(Color color, Coordinate cord) {
        List<Coordinate> initialMoves = new ArrayList<>();
        initialMoves.add(new Coordinate(1, 0));
        initialMoves.add(new Coordinate(0, 1));
        initialMoves.add(new Coordinate(1, 1));
        initialMoves.add(new Coordinate(0, -1));
        initialMoves.add(new Coordinate(-1, -1));
        initialMoves.add(new Coordinate(-1, -1));
        //initialMoves.add(new Coordinate(0, 2));
        //initialMoves.add(new Coordinate(0, -2));
        //todo castling
        return transformMoves(initialMoves, color, cord);
    }

    private List<Coordinate> getKnightMoves(Color color, Coordinate cord) {
        List<Coordinate> initialMoves = new ArrayList<>();
        initialMoves.add(new Coordinate(2, 1));
        initialMoves.add(new Coordinate(2, -1));
        initialMoves.add(new Coordinate(1, 2));
        initialMoves.add(new Coordinate(1, -2));
        initialMoves.add(new Coordinate(-2, 1));
        initialMoves.add(new Coordinate(-2, -1));
        initialMoves.add(new Coordinate(-1, 2));
        initialMoves.add(new Coordinate(-1, -2));
        return transformMoves(initialMoves, color, cord);
    }

    private List<Coordinate> getQueenMoves(Color color, Coordinate cord) {
        List<Coordinate> initialMoves = new ArrayList<>();
        IntStream.range(0, board.getSize()).forEach(i -> {
            initialMoves.add(new Coordinate(i, 0));
            initialMoves.add(new Coordinate(0, i));
            initialMoves.add(new Coordinate(i, i));
            initialMoves.add(new Coordinate(-i, 0));
            initialMoves.add(new Coordinate(0, -i));
            initialMoves.add(new Coordinate(-i, -i));
        });

        return filterOverjump(transformMoves(initialMoves, color, cord), cord);
    }

    private List<Coordinate> getRookMoves(Color color, Coordinate cord) {
        List<Coordinate> initialMoves = new ArrayList<>();
        IntStream.range(0, board.getSize()).forEach(i -> {
            initialMoves.add(new Coordinate(i, 0));
            initialMoves.add(new Coordinate(0, i));
            initialMoves.add(new Coordinate(-i, 0));
            initialMoves.add(new Coordinate(0, -i));
        });

        //todo Castling
        return filterOverjump(transformMoves(initialMoves, color, cord), cord);
    }

    private List<Coordinate> getPawnMoves(Color color, Coordinate cord) {
        List<Coordinate> initialMoves = new ArrayList<>();
        if (board.getCell(cord).getFigure().map(Figure::getMoveCount).orElse(0) == 0) {
            initialMoves.add(new Coordinate(2, 0));
        }
        initialMoves.add(new Coordinate(1, 0));
        initialMoves.add(new Coordinate(1, 1));
        initialMoves.add(new Coordinate(1, -1));

        List<Coordinate> moves = transformMoves(initialMoves, color, cord);
        moves = moves.stream().filter(c -> {
                    //todo enpassant
            boolean attack = c.getCol() != cord.getCol() && board.getCell(c).getFigure().isPresent() && board.getCell(c).getFigure().get().getColor() != color;
            boolean firstMove = Math.abs(c.getRow() - cord.getRow()) == 2 && board.getCell(cord).getFigure().get().getMoveCount() == 0 && !board.getCell((cord.getRow() + c.getRow()) / 2, c.getCol()).getFigure().isPresent();
            boolean regular = Math.abs(c.getRow() - cord.getRow()) == 1 && c.getCol() == cord.getCol() && !board.getCell(c).getFigure().isPresent();
            return attack || firstMove || regular;
                }
        ).collect(Collectors.toList());

        return moves;
    }

    private List<Coordinate> transformMoves(List<Coordinate> moves, Color color, Coordinate cord) {
        return moves.stream()
                .distinct()
                .map(m -> m.add(color, cord))
                .filter(m -> m.getRow() >= 0 && m.getRow() < board.getSize() && m.getCol() >= 0 && m.getCol() < board.getSize())
                .filter(m -> board.getCell(m).getFigure().map(f -> f.getColor() != color).orElse(true))
                .collect(toList());
    }

    private List<Coordinate> filterOverjump(List<Coordinate> moves, Coordinate cord) {
        return moves.stream().filter(m -> !isBetween(m, cord)).collect(toList());
    }

    private boolean isBetween(Coordinate src, Coordinate dst) {
        if (src.getRow() == dst.getRow()) {
            return IntStream.range(Math.min(src.getCol(), dst.getCol()), Math.max(src.getCol(), dst.getCol())).anyMatch(col ->
                    !(src.getCol() == col || !board.getCell(src.getRow(), col).getFigure().isPresent()));
        } else if (src.getCol() == dst.getCol()) {
            return IntStream.range(Math.min(src.getRow(), dst.getRow()), Math.max(src.getRow(), dst.getRow())).anyMatch(row ->
                    !(src.getRow() == row || !board.getCell(row, src.getCol()).getFigure().isPresent()));
        } else {
            return IntStream.range(Math.min(src.getRow(), dst.getRow()), Math.max(src.getRow(), dst.getRow())).anyMatch(row ->
                    IntStream.range(Math.min(src.getCol(), dst.getCol()), Math.max(src.getCol(), dst.getCol())).anyMatch(col ->
                            !(src.getRow() == row && src.getCol() == col || !board.getCell(row, col).getFigure().isPresent()))
            );
        }
    }

    public void recalculateBoard() {
        board.forEach(c -> {
            List<Coordinate> possibleMoves = c.getFigure().map(f -> getPossibleMoves(c.getCoordinate())).orElse(Collections.emptyList());
            c.addPossibleMoves(possibleMoves);
            possibleMoves.stream().forEach(m -> board.getCell(m).addAttackedBy(c.getCoordinate()));
        });
    }
}
