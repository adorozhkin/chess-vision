package org.adorozhkin.chessvision.model;

import java.util.concurrent.atomic.AtomicInteger;

public class Figure {
    private final Color color;
    private final Figures type;
    private final AtomicInteger moves = new AtomicInteger(0);

    public Figure(Color color, Figures type) {
        this.color = color;
        this.type = type;
    }

    public Color getColor() {
        return color;
    }

    public Figures getType() {
        return type;
    }

    public int getMoveCount() {
        return moves.get();
    }

    public void incMoveCount() {
        moves.incrementAndGet();
    }

    @Override
    public String toString() {
        return color.toString() + type.toString();
    }
}
