package org.adorozhkin.chessvision.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

public class Cell {
    private Optional<Figure> figure = Optional.empty();
    private final Coordinate coordinate;
    private volatile List<Coordinate> attackedBy = new CopyOnWriteArrayList<>();
    private volatile List<Coordinate> possibleMoves = new CopyOnWriteArrayList<>();

    public Cell(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Cell(int row, int col) {
        this.coordinate = new Coordinate(row, col);
    }

    public Optional<Figure> getFigure() {
        return figure;
    }

    public void setFigure(Figure figure) {
        this.figure = Optional.of(figure);
    }

    public Collection<Coordinate> getAttackedBy() {
        return attackedBy;
    }

    public Collection<Coordinate> getPossibleMoves() {
        return possibleMoves;
    }

    public void addAttackedBy(Coordinate... attackers) {
        Arrays.stream(attackers).forEach(a -> attackedBy.add(a));
    }

    public void addPossibleMoves(List<Coordinate> moves) {
        possibleMoves.addAll(moves);
    }

    public void clear() {
        attackedBy.clear();
        possibleMoves.clear();
    }

    public int getCol() {
        return coordinate.getCol();
    }

    public int getRow() {
        return coordinate.getRow();
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "figure=" + figure +
                ", coordinate=" + coordinate +
                '}';
    }
}
