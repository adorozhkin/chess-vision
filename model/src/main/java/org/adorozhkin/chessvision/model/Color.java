package org.adorozhkin.chessvision.model;

public enum Color {
    BLACK(-1), WHITE(1);

    private final int direction;

    Color(int direction) {
        this.direction = direction;
    }

    public int getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return direction > 0 ? "W" : "B";
    }
}
