package org.adorozhkin.chessvision.model;

import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.function.Consumer;
import java.util.stream.IntStream;

public class Board {
    private static final int DEFAULT_SIZE = 8;
    private final int size;
    private final AtomicReferenceArray<Cell>[] cells;

    public Board() {
        this(DEFAULT_SIZE);
    }

    public Board(int size) {
        this.size = size;
        cells = new AtomicReferenceArray[size];
        IntStream.range(0, size).forEach(i -> cells[i] = new AtomicReferenceArray<>(size));
        initCells();
    }

    public int getSize() {
        return size;
    }

    public Cell getCell(Coordinate cords) {
        return getCell(cords.getRow(), cords.getCol());
    }

    public Cell getCell(int row, int col) {
        return cells[row].get(col);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        forEach(c -> c.getFigure()
                .map(f -> s.append(f.toString()))
                .orElseGet(() -> s.append("   "))
                .append(c.getCol() == size - 1 ? "\n" : ""));
        return s.toString();
    }

    public static Board createDefaultBoard() {
        Board b = new Board();
        IntStream.range(0, b.getSize()).forEach(i -> b.getCell(1, i).setFigure(new Figure(Color.WHITE, Figures.PAWN)));
        IntStream.range(0, b.getSize()).forEach(i -> b.getCell(b.getSize() - 2, i).setFigure(new Figure(Color.BLACK, Figures.PAWN)));

        b.getCell(0, 0).setFigure(new Figure(Color.WHITE, Figures.ROOK));
        b.getCell(0, b.getSize() - 1).setFigure(new Figure(Color.WHITE, Figures.ROOK));
        b.getCell(b.getSize() - 1, 0).setFigure(new Figure(Color.BLACK, Figures.ROOK));
        b.getCell(b.getSize() - 1, b.getSize() - 1).setFigure(new Figure(Color.BLACK, Figures.ROOK));

        b.getCell(0, 1).setFigure(new Figure(Color.WHITE, Figures.KNIGHT));
        b.getCell(0, b.getSize() - 2).setFigure(new Figure(Color.WHITE, Figures.KNIGHT));
        b.getCell(b.getSize() - 1, 1).setFigure(new Figure(Color.BLACK, Figures.KNIGHT));
        b.getCell(b.getSize() - 1, b.getSize() - 2).setFigure(new Figure(Color.BLACK, Figures.KNIGHT));

        b.getCell(0, 2).setFigure(new Figure(Color.WHITE, Figures.BISHOP));
        b.getCell(0, b.getSize() - 3).setFigure(new Figure(Color.WHITE, Figures.BISHOP));
        b.getCell(b.getSize() - 1, 2).setFigure(new Figure(Color.BLACK, Figures.BISHOP));
        b.getCell(b.getSize() - 1, b.getSize() - 3).setFigure(new Figure(Color.BLACK, Figures.BISHOP));

        b.getCell(0, 3).setFigure(new Figure(Color.WHITE, Figures.QUEEN));
        b.getCell(0, b.getSize() - 4).setFigure(new Figure(Color.WHITE, Figures.KING));
        b.getCell(b.getSize() - 1, 3).setFigure(new Figure(Color.BLACK, Figures.QUEEN));
        b.getCell(b.getSize() - 1, b.getSize() - 4).setFigure(new Figure(Color.BLACK, Figures.KING));

        return b;
    }

    public void forEach(Consumer<Cell> f) {
        IntStream.range(0, size).forEach(row ->
                IntStream.range(0, size).forEach(col ->
                        f.accept(getCell(row, col)))
        );
    }

    private void initCells() {
        IntStream.range(0, size).forEach(row ->
                IntStream.range(0, size).forEach(col ->
                        cells[row].set(col, new Cell(row, col))));
    }

    public void resetCells() {
        forEach(c -> c.clear());
    }

    public void move(Coordinate from, Coordinate to) {
        getCell(to).setFigure(getCell(from).getFigure().get());
        getCell(from).setFigure(null);
    }

    public void setFigure(Coordinate cord, Figure figure) {
        getCell(cord).setFigure(figure);
    }
}
