package org.adorozhkin.chessvision.model;

public enum Figures {
    PAWN(1), KNIGHT(3), BISHOP(4), ROOK(5), QUEEN(10), KING(4);

    private final int weight;

    Figures(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        switch (this) {
            case BISHOP:
                return "B";
            case KING:
                return "K";
            case KNIGHT:
                return "k";
            case PAWN:
                return "p";
            case QUEEN:
                return "Q";
            case ROOK:
                return "R";
        }
        return " ";
    }
}
