package org.adorozhkin.chessvision.rules;

import org.adorozhkin.chessvision.model.*;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.*;

public class MovementRuleTest {
    @Test
    public void defaultBoard() {
        Board b = Board.createDefaultBoard();
        assertEquals("Default boarder", "WRWkWBWQWKWBWkWR\n" +
                "WpWpWpWpWpWpWpWp\n" +
                "                        \n" +
                "                        \n" +
                "                        \n" +
                "                        \n" +
                "BpBpBpBpBpBpBpBp\n" +
                "BRBkBBBQBKBBBkBR\n", b.toString());
        b.forEach(c -> assertTrue(c + " no possible moves", c.getPossibleMoves().isEmpty()));
        b.forEach(c -> assertTrue(c + " no attacked by", c.getAttackedBy().isEmpty()));
        b.forEach(c -> assertNotNull(c + " has coordinates", c.getCoordinate()));

        b.forEach(c -> {
            if (c.getRow() < 2 || c.getRow() > b.getSize() - 3) {
                assertTrue(c + " has figure", c.getFigure().isPresent());
            } else {
                assertFalse(c + " has figure", c.getFigure().isPresent());
            }
        });
    }

    @Test
    public void processBoard() {
        Board b = Board.createDefaultBoard();

        b.forEach(c -> assertTrue(c + " no possible moves", c.getPossibleMoves().isEmpty()));
        b.forEach(c -> assertTrue(c + " no attacked by", c.getAttackedBy().isEmpty()));

        new MovementRule(b).recalculateBoard();

        b.forEach(c -> {
            if (c.getRow() == 1 || c.getRow() == b.getSize() - 2
                    || (c.getCol() == 1 || c.getCol() == b.getSize() - 2) && (c.getRow() == 0 || c.getRow() == b.getSize() - 1)) {
                assertFalse(c + " has possible moves", c.getPossibleMoves().isEmpty());
            } else {
                assertTrue(c + " has no possible moves", c.getPossibleMoves().isEmpty());
            }

            if (c.getRow() == 2 || c.getRow() == 3 || c.getRow() == b.getSize() - 3 || c.getRow() == b.getSize() - 4) {
                assertFalse(c + " attacked by", c.getAttackedBy().isEmpty());
            }
        });
    }

    @Test
    public void pawnMoves() {
        Board b = Board.createDefaultBoard();
        b.setFigure(new Coordinate(2, 0), new Figure(Color.WHITE, Figures.PAWN));
        b.setFigure(new Coordinate(2, 3), new Figure(Color.BLACK, Figures.PAWN));
        new MovementRule(b).recalculateBoard();

        Collection<Coordinate> possibleMoves = b.getCell(1, 0).getPossibleMoves();
        assertTrue("No moves", possibleMoves.isEmpty());

        possibleMoves = b.getCell(1, 1).getPossibleMoves();
        assertEquals("No eat own", 2, possibleMoves.size());

        possibleMoves = b.getCell(1, 2).getPossibleMoves();
        assertEquals("Eat", 3, possibleMoves.size());
    }
}
